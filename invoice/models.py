from django.db import models

# Create your models here.
from profiles.models import Client, Waiter
from service.models import Table, Product


class Order(models.Model):
    reference = models.CharField(max_length=100, null=False, unique=True)

    def __str__(self):
        return self.reference


class OrderDetail(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(null=True)

    def __str__(self):
        return self.order.reference


class Invoice(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    waiter = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    table = models.ForeignKey(Table, on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True)
    order = models.OneToOneField(Order, on_delete=models.CASCADE)
