from rest_framework import serializers

from invoice.models import Order, OrderDetail, Invoice


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['url', 'reference']


class OrderDetailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = OrderDetail
        fields = '__all__'


class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = ['url', 'client', 'waiter', 'table' 'date', 'order']
