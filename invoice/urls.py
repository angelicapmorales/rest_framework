from django.urls import path
from rest_framework import routers

from invoice.views import Home, AddOrderView, UpdateOrderView, ListOrderView, DeleteOrderView, AddOrderDetailView, \
    UpdateOrderDetailView, ListOrderDetailView, DeleteOrderDetailView, AddInvoiceView, UpdateInvoiceView, \
    ListInvoiceView, DeleteInvoiceView, OrderViewSet, OrderDetailViewSet, InvoiceViewSet

router = routers.DefaultRouter()
router.register(r'order', OrderViewSet)
router.register(r'order_detail', OrderDetailViewSet)
router.register(r'invoice', InvoiceViewSet)

urlpatterns = [
    path('', Home.as_view(), name='Home'),
    path('add_order', (AddOrderView.as_view()), name='add_order'),
    path('update_order/<int:pk>/', (UpdateOrderView.as_view()), name='update_order'),
    path('list_order', (ListOrderView.as_view()), name='list_order'),
    path('delete_order/<int:pk>/', (DeleteOrderView.as_view()), name='delete_order'),

    path('add_order_detail', (AddOrderDetailView.as_view()), name='add_order_detail'),
    path('update_order_detail/<int:pk>/', (UpdateOrderDetailView.as_view()), name='update_order_detail'),
    path('list_order_detail', (ListOrderDetailView.as_view()), name='list_order_detail'),
    path('delete_order_detail/<int:pk>/', (DeleteOrderDetailView.as_view()), name='delete_order_detail'),

    path('add_invoice', (AddInvoiceView.as_view()), name='add_invoice'),
    path('update_invoice/<int:pk>/', (UpdateInvoiceView.as_view()), name='update_invoice'),
    path('list_invoice', (ListInvoiceView.as_view()), name='list_invoice'),
    path('delete_invoice/<int:pk>/', (DeleteInvoiceView.as_view()), name='delete_invoice'),
]
