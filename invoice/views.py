from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView, CreateView, UpdateView, ListView, DeleteView
from rest_framework import viewsets, permissions

from invoice.forms import OrderForm, OrderDetailForm, InvoiceForm
from invoice.models import Order, OrderDetail, Invoice
from invoice.serializer import OrderSerializer, OrderDetailSerializer, InvoiceSerializer


class Home(TemplateView):
    template_name = 'home.html'


# ORDER
class AddOrderView(CreateView):
    model = Order
    form_class = OrderForm
    template_name = 'invoice/order/register_order.html'
    success_url = '/list_order'


class UpdateOrderView(UpdateView):
    model = Order
    form_class = OrderForm
    pk_url_kwarg = 'pk'
    template_name = 'invoice/order/register_order.html'
    success_url = '/list_order'


class ListOrderView(ListView):
    model = Order
    queryset = Order.objects.all()
    template_name = 'invoice/order/list_order.html'
    context_object_name = 'order_list'


class DeleteOrderView(DeleteView):
    model = Order
    pk_url_kwarg = 'pk'
    template_name = 'invoice/order/delete_order.html'
    success_url = '/list_order'


# ORDER_DETAIL

class AddOrderDetailView(CreateView):
    model = OrderDetail
    form_class = OrderDetailForm
    template_name = 'invoice/order_detail/register_order_detail.html'
    success_url = '/list_order_detail'


class UpdateOrderDetailView(UpdateView):
    model = OrderDetail
    form_class = OrderDetailForm
    pk_url_kwarg = 'pk'
    template_name = 'invoice/order_detail/register_order_detail.html'
    success_url = '/list_order_detail'


class ListOrderDetailView(ListView):
    model = OrderDetail
    queryset = OrderDetail.objects.all()
    template_name = 'invoice/order_detail/list_order_detail.html'
    context_object_name = 'order_detail_list'


class DeleteOrderDetailView(DeleteView):
    model = OrderDetail
    pk_url_kwarg = 'pk'
    template_name = 'invoice/order_detail/delete_order_detail.html'
    success_url = '/list_order_detail'


# INVOICE
class AddInvoiceView(CreateView):
    model = Invoice
    form_class = InvoiceForm
    template_name = 'invoice/invoice/register_invoice.html'
    success_url = '/list_invoice'


class UpdateInvoiceView(UpdateView):
    model = Invoice
    form_class = InvoiceForm
    pk_url_kwarg = 'pk'
    template_name = 'invoice/invoice/register_invoice.html'
    success_url = '/list_invoice'


class ListInvoiceView(ListView):
    model = Invoice
    queryset = Invoice.objects.all()
    template_name = 'invoice/invoice/list_invoice.html'
    context_object_name = 'invoice_list'


class DeleteInvoiceView(DeleteView):
    model = Invoice
    pk_url_kwarg = 'pk'
    template_name = 'invoice/invoice/delete_invoice.html'
    success_url = '/list_invoice'


# RESTVIEW
class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [permissions.IsAuthenticated]


class OrderDetailViewSet(viewsets.ModelViewSet):
    queryset = OrderDetail.objects.all()
    serializer_class = OrderDetailSerializer
    permission_classes = [permissions.IsAuthenticated]


class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer
    permission_classes = [permissions.IsAuthenticated]
