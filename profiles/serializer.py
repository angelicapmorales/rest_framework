from rest_framework import serializers

from profiles.models import Client, Waiter


class ClientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ['url', 'first_name', 'last_name', 'observations']


class WaiterSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Waiter
        fields = ['url', 'first_name', 'last_name1', 'last_name2']


