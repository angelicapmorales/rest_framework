from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Client(models.Model):
    first_name = models.CharField(max_length=100, null=False)
    last_name = models.CharField(max_length=100, null=False)
    observations = models.CharField(max_length=200, null=False)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Waiter(models.Model):
    first_name = models.CharField(max_length=100, null=False)
    last_name1 = models.CharField(max_length=100, null=False)
    last_name2 = models.CharField(max_length=200, null=False)

    def __str__(self):
        return f'{self.first_name} {self.last_name1}'
