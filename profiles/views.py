from django.shortcuts import render

# Create your views here.
from django.views.generic import CreateView, UpdateView, ListView, DeleteView
from rest_framework import viewsets, permissions

from profiles.forms import ClientForm, WaiterForm
from profiles.models import Client, Waiter

# CLIENT
from profiles.serializer import ClientSerializer, WaiterSerializer


class AddClientView(CreateView):
    model = Client
    form_class = ClientForm
    template_name = 'profiles/client/register_client.html'
    success_url = '/list_client'


class UpdateClientView(UpdateView):
    model = Client
    form_class = ClientForm
    pk_url_kwarg = 'pk'
    template_name = 'profiles/client/register_client.html'
    success_url = '/list_client'


class ListClientView(ListView):
    model = Client
    queryset = Client.objects.all()
    template_name = 'profiles/client/list_client.html'
    context_object_name = 'client_list'


class DeleteClientView(DeleteView):
    model = Client
    pk_url_kwarg = 'pk'
    template_name = 'profiles/client/delete_client.html'
    success_url = '/list_client'


# WAITER

class AddWaiterView(CreateView):
    model = Waiter
    form_class = WaiterForm
    template_name = 'profiles/waiter/register_waiter.html'
    success_url = '/list_waiter'


class UpdateWaiterView(UpdateView):
    model = Waiter
    form_class = WaiterForm
    pk_url_kwarg = 'pk'
    template_name = 'profiles/waiter/register_waiter.html'
    success_url = '/list_waiter'


class ListWaiterView(ListView):
    model = Waiter
    queryset = Waiter.objects.all()
    template_name = 'profiles/waiter/list_waiter.html'
    context_object_name = 'waiter_list'


class DeleteWaiterView(DeleteView):
    model = Waiter
    pk_url_kwarg = 'pk'
    template_name = 'profiles/waiter/delete_waiter.html'
    success_url = '/list_waiter'


# RESTVIEW

class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = [permissions.IsAuthenticated]


class WaiterViewSet(viewsets.ModelViewSet):
    queryset = Waiter.objects.all()
    serializer_class = WaiterSerializer
    permission_classes = [permissions.IsAuthenticated]

