from django.urls import path
from rest_framework import routers

from profiles.views import AddClientView, UpdateClientView, ListClientView, DeleteClientView, AddWaiterView, \
    UpdateWaiterView, ListWaiterView, DeleteWaiterView, ClientViewSet, WaiterViewSet

router = routers.DefaultRouter()
router.register(r'client', ClientViewSet)
router.register(r'waiter', WaiterViewSet)


urlpatterns = [
    path('add_client', (AddClientView.as_view()), name='add_client'),
    path('update_client/<int:pk>/', (UpdateClientView.as_view()), name='update_client'),
    path('list_client', (ListClientView.as_view()), name='list_client'),
    path('delete_client/<int:pk>/', (DeleteClientView.as_view()), name='delete_client'),

    path('add_waiter', (AddWaiterView.as_view()), name='add_waiter'),
    path('update_waiter/<int:pk>/', (UpdateWaiterView.as_view()), name='update_waiter'),
    path('list_waiter', (ListWaiterView.as_view()), name='list_waiter'),
    path('delete_waiter/<int:pk>/', (DeleteWaiterView.as_view()), name='delete_waiter'),
]
