from django.contrib import admin

# Register your models here.
from profiles.models import Client, Waiter

admin.site.register(Client)
admin.site.register(Waiter)

