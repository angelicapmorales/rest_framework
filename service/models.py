from django.db import models


# Create your models here.
class ProductType(models.Model):
    type = models.CharField(max_length=50, null=False)

    def __str__(self):
        return self.type


class Product(models.Model):
    name = models.CharField(max_length=45, null=False)
    price = models.IntegerField(null=False)
    product_type = models.ForeignKey(ProductType, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Table(models.Model):
    diners_num = models.IntegerField(null=False)
    location = models.CharField(max_length=45, null=False)

    def __str__(self):
        return self.location
