from django.shortcuts import render

# Create your views here.
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from rest_framework import viewsets, permissions

from service.forms import ProductForm, ProductTypeForm, TableForm
from service.models import Product, ProductType, Table

# PRODUCTS TYPE
from service.serializer import ProductTypeSerializer, ProductSerializer, TableSerializer


class AddProductTypeView(CreateView):
    model = ProductType
    form_class = ProductTypeForm
    template_name = 'service/product_type/register_product_type.html'
    success_url = '/list_product_type'


class ListProductTypeView(ListView):
    model = ProductType
    queryset = ProductType.objects.all()
    template_name = 'service/product_type/list_product_type.html'
    context_object_name = 'product_type_list'


class UpdateProductTypeView(UpdateView):
    model = ProductType
    form_class = ProductTypeForm
    pk_url_kwarg = 'pk'
    template_name = 'service/product_type/register_product_type.html'
    success_url = '/list_product_type'


class DeleteProductTypeView(DeleteView):
    model = ProductType
    pk_url_kwarg = 'pk'
    template_name = 'service/product_type/delete_product_type.html'
    success_url = '/list_product_type'


# PRODUCTS

class AddProductView(CreateView):
    model = Product
    form_class = ProductForm
    template_name = 'service/product/register_product.html'
    success_url = '/list_product'


class UpdateProductView(UpdateView):
    model = Product
    form_class = ProductForm
    pk_url_kwarg = 'pk'
    template_name = 'service/product/register_product.html'
    success_url = '/list_product'


class ListProductView(ListView):
    model = Product
    queryset = Product.objects.all()
    template_name = 'service/product/list_products.html'
    context_object_name = 'product_list'


class DeleteProductView(DeleteView):
    model = Product
    pk_url_kwarg = 'pk'
    template_name = 'service/product/delete_product.html'
    success_url = '/list_product'


# TABLE

class AddTableView(CreateView):
    model = Table
    form_class = TableForm
    template_name = 'service/table/register_table.html'
    success_url = '/list_table'


class UpdateTableView(UpdateView):
    model = Table
    form_class = TableForm
    pk_url_kwarg = 'pk'
    template_name = 'service/table/register_table.html'
    success_url = '/list_table'


class ListTableView(ListView):
    model = Table
    queryset = Table.objects.all()
    template_name = 'service/table/list_table.html'
    context_object_name = 'table_list'


class DeleteTableView(DeleteView):
    model = Table
    pk_url_kwarg = 'pk'
    template_name = 'service/table/delete_table.html'
    success_url = '/list_table'


# RESTVIEW
class ProductTypeViewSet(viewsets.ModelViewSet):
    queryset = ProductType.objects.all()
    serializer_class = ProductTypeSerializer
    permission_classes = [permissions.IsAuthenticated]


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticated]


class TableViewSet(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer
    permission_classes = [permissions.IsAuthenticated]
