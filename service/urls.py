from django.urls import path
from rest_framework import routers

from service.views import AddProductView, UpdateProductView, ListProductView, DeleteProductView, AddProductTypeView, \
    UpdateProductTypeView, ListProductTypeView, DeleteProductTypeView, AddTableView, UpdateTableView, ListTableView, \
    DeleteTableView, ProductTypeViewSet, ProductViewSet, TableViewSet

router = routers.DefaultRouter()
router.register(r'product_type', ProductTypeViewSet)
router.register(r'product', ProductViewSet)
router.register(r'table', TableViewSet)


urlpatterns = [
    path('add_product_type', (AddProductTypeView.as_view()), name='add_product_type'),
    path('update_product_type/<int:pk>/', (UpdateProductTypeView.as_view()), name='update_product_type'),
    path('list_product_type', (ListProductTypeView.as_view()), name='list_product_type'),
    path('delete_product_type/<int:pk>/', (DeleteProductTypeView.as_view()), name='delete_product_type'),

    path('add_product', (AddProductView.as_view()), name='add_product'),
    path('update_product/<int:pk>/', (UpdateProductView.as_view()), name='update_product'),
    path('list_product', (ListProductView.as_view()), name='list_product'),
    path('delete_product/<int:pk>/', (DeleteProductView.as_view()), name='delete_product'),

    path('add_table', (AddTableView.as_view()), name='add_table'),
    path('update_table/<int:pk>/', (UpdateTableView.as_view()), name='update_table'),
    path('list_table', (ListTableView.as_view()), name='list_table'),
    path('delete_table/<int:pk>/', (DeleteTableView.as_view()), name='delete_table'),
]
