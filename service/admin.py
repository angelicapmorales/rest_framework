from django.contrib import admin

# Register your models here.
from service.models import Table, Product, ProductType

admin.site.register(ProductType)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'price', 'product_type')


admin.site.register(Table)
