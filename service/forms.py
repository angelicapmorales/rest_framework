from django import forms
from service.models import ProductType, Product, Table


class ProductTypeForm(forms.ModelForm):
    class Meta:
        model = ProductType
        fields = '__all__'


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = '__all__'


class TableForm(forms.ModelForm):
    class Meta:
        model = Table
        fields = '__all__'
