from rest_framework import serializers

from service.models import Product, Table, ProductType


class ProductTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductType
        fields = ['url', 'type']


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['url', 'name', 'price', 'product_type']


class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model: Table
        fields = ['url', 'diners_num', 'location']

